#!/usr/bin/env python
# coding: utf-8

# Requirements:
# pip install elasticsearch
# pip install every other moculde that python needs
# 
# How do we package it up so the python script just runs and you don't need things? Is that possible? Maybe make it a docker image?

# In[ ]:


from elasticsearch import Elasticsearch
from datetime import date
import pandas as pd

logstash_index = 'logstash-' + date.today().strftime('%Y-%m-%-d')
es = Elasticsearch(['localhost:9200/'])


# In[ ]:


def get_elasticsearch_json(logstash_index, filename="*", type="*"):
    scrolls = []
    doc = {
           "size": 10000,
          "query": { 
              "bool": 
                  {
                  "must": [
                    {
                    "wildcard" : {
                        "filename" : filename,
                    }
                  },
                    {
                    "wildcard" : {
                        "type" : type,
                    }
                  }
                  ]
                }
              
            },
          "_source": ["@timestamp", 'logdate', "message", "type", "filename"]
        }
    response = es.search(index=logstash_index, body=doc, scroll='5m')
    scrolls.append(response)
    # need to do scrolling when the size is above 10000
    scroll_id = response['_scroll_id']
    scroll_size = response['hits']['total']
#     while scroll_size > 0:
    n = 0
    while scroll_size > 0:
        new_response= es.scroll(scroll_id = scroll_id, scroll = '5m')
        scrolls.append(new_response)
        
        # set the new scroll
        scroll_id = new_response['_scroll_id']
        scroll_size = scroll_size - 10000
        n = n + 1
    return scrolls


# In[ ]:


def json_to_pandas(es_result):
    message = []
    date = []
    types = []
    filenames = []
    logfile = pd.DataFrame()
    # could turn this for loop into a .apply(getdates) sort of thing
    for entry in es_result['hits']['hits']:
        tmpmessage = entry['_source']['message']
        if type(tmpmessage) == list:
            message.append(tmpmessage[0])
        else:
            message.append(tmpmessage)
        date.append(entry['_source']['@timestamp'])
        filenames.append(entry['_source']['filename'])
        types.append(entry['_source']['type'])
    logfile['message'] = message
    logfile['date'] = date
    logfile['filename'] = filenames
    logfile['type'] = types
    return logfile


# In[ ]:


response_array = get_elasticsearch_json(logstash_index) #can input log names as a parameter to only return lines from that log

log_collection = pd.DataFrame()
for scroll in response_array:
    log_collection = log_collection.append(json_to_pandas(scroll), ignore_index=True)


# In[ ]:


log_collection

