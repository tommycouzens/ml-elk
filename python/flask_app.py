# import the necessary packages
import flask
from flask import request
# import io
# import os
import numpy as np
from datetime import date
# from werkzeug.utils import secure_filename
from elasticsearch_to_python import get_elasticsearch_json
from elasticsearch_to_python import json_to_pandas
import elasticsearch_to_python
import log_novelty_detection

# initialize our Flask application and the Keras model
app = flask.Flask(__name__)
app.config['UPLOAD_FOLDER'] = '../elk/logstash/logs'
tf_avg_v1 = None
test_collection = None

@app.route("/")
def hello():
    return "Hello! This is a flask app that can be used to run the log outlier machine learning algorithm.\n \
You can use \033[1mlocalhost:4000/post_file\033[0m\033[50m to send a file to elasticsearch, or \033[1mlocalhost:4000/print_file\033[0m\033[50m to get your outliers! \n"

@app.route("/send")
def send():
    return "I'd like to use this to send logs to elasticsearch but haven't coded it yet"

@app.route("/train")
def train():
    logstash_index = 'logstash-' + date.today().strftime('%Y-%m-%-d')
    response_array = get_elasticsearch_json(logstash_index)
    log_collection = log_novelty_detection.prepare_data_es(response_array)
    global tf_avg_v1, test_collection
    test_collection, tf_avg_v1 = log_novelty_detection.train_outlier_model(log_collection)
    return "Trained with " + str(log_collection.shape[0]) + " lines of logs \n"

@app.route("/result")
def result():
    # global tf_avg_v1
    if tf_avg_v1:
        outlier_indexes = np.argsort(tf_avg_v1)
        result = "\033[1mLOG NOVELTIES:\033[0m\033[50m\n"
        for i in range(5):
            result = result +("\033[1mWeight\033[0m\033[50m: " + str(round(tf_avg_v1[i],2)) + "\n\033[1mMessage\033[0m\033[50m: " + test_collection['message'].iloc[i]) + '\n\n'
    else:
        result = "You need to train first!\n"
    return result

# Start the flask server
if __name__ == "__main__":
    app.run(port=4000)


# @app.route("/post_file", methods=['POST','PUT'])
# def post_file():
#     file = request.files['file']
#     filename=secure_filename(file.filename) 
#     file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
#     return filename

# @app.route("/print_file", methods=['GET','POST','PUT'])
# def print_file():
#     file = open("upload/testfile", "r")
#     print(file.read())
#     hello = file.read()
#     return hello