#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import datetime
import re
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from elasticsearch import Elasticsearch
import pandas as pd
from datetime import date

from elasticsearch_to_python import get_elasticsearch_json
from elasticsearch_to_python import json_to_pandas


# In[ ]:


# uses the function json_to_pandas defined in get_elasticsearch_json.py

def prepare_data_es(response_array):
    logs = pd.DataFrame()
    for scroll in response_array:
        logs = logs.append(json_to_pandas(scroll), ignore_index=True)

    # replace any strings with nan then remove
    logs.replace('', np.nan, inplace=True)
    logs.dropna(inplace= True)
    return logs


# In[ ]:


def prepare_data_cv(log_collection):
    # custom token pattern ensures we tokenise single characters as well as punctuation
    count_vectorizer = CountVectorizer(analyzer='word', ngram_range=(1,2), token_pattern=r"(?u)\b\w+\b|[{}\[\],:!.;()=>]+")
    # join all messages together into a single document - we want a definitive vocab for the model
    messages = pd.Series(' '.join(log_collection['message'].tolist()))
    data = count_vectorizer.fit_transform(messages)
    vocab = sorted(count_vectorizer.vocabulary_)
    
    return log_collection, messages, data, vocab, count_vectorizer


# In[ ]:


def test_data(log_collection, count_vectorizer):
#     log_collection = log_collection.sort_values('date')
#     log_collection = log_collection.reset_index()
#     log_collection = log_collection.drop(columns=['data'])
    data = count_vectorizer.transform(log_collection['message'])
    
    return log_collection, data


# In[ ]:


def train_outlier_model(log_collection):
    train_collection = log_collection[:int(log_collection.shape[0] * 0.7)]
    test_collection = log_collection[-int(log_collection.shape[0] * 0.3):]
    train_collection, train_messages, X_train, vocab, count_vectorizer = prepare_data_cv(train_collection)
    test_collection, X_test = test_data(test_collection, count_vectorizer)
    
    tf_avg_v1 = []
    for i, x in enumerate(X_test):
        #over_zero_indexes = np.where( X_test[i] != 0 )[0]
        over_zero_indexes = X_test[i].nonzero()[0]
        num_tokens = ( len(re.findall(r"(?u)\b\w+\b|[{}\[\],:!.;()=>]+", ''.join(test_collection['message'].iloc[i]))) * 2 ) -1
        #tf_avg_v1.append(float(np.sum(X_test[i])) / float(len(test_collection['message'].iloc[i].split())))
        tf_avg_v1.append(float(np.sum(X_test[i])) / (float(num_tokens) - float(len(over_zero_indexes)) + 1))
    return test_collection, tf_avg_v1


# In[ ]:


def outlier_model_results(test_collection, tf_avg_v1, length):
    outlier_indexes = np.argsort(tf_avg_v1)
    print("\033[1mLOG NOVELTIES:\033[0m\033[50m\n")
    for i in outlier_indexes[:5]:
        print("\033[1mWeight\033[0m\033[50m: " + str(round(tf_avg_v1[i],2))) 
        print("\033[1mMessage:\033[0m\033[50m " + test_collection['message'].iloc[i])
        print ("")


# In[ ]:


# logstash_index = 'logstash-' + date.today().strftime('%Y-%m-%-d')

# response_array = get_elasticsearch_json(logstash_index)
# log_collection = prepare_data_es(response_array)
# test_collection, tf_avg_v1 = train_outlier_model(log_collection)

# outlier_model_results(test_collection, tf_avg_v1, 5)


# In[ ]:




