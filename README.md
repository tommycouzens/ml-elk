# Machine Learning ELK:
This repository allows the transformation logs into a format readable by python Machine Learning algorithms such as the example outlier algorithm given.

ELK stands for Elasticsearch, Logstash and Kibana:
https://www.elastic.co/elk-stack

First it launches the ELK stack in docker images. Logstash transforms the logs, using grok filters to split the logs into a logdate / message format, and then sends them to elasticsearch. The log outlier script can then be called by running it in python manually, or running the flask app and using it's api to send a request and recieve the output.

![elk-diagram](ml-elk.png)

By using logstash at the start and flask at the end an enginner should be able to send just two commands to send their logs through the process and then get the output of the algorithm from the server.

This setup could be used for multiple-nodes allowing for scalability over bottlenecks in any part of the process. (is this true? Ask Maartens about it)

# Starting the ELK stack

## Installing docker

You should have python installed, ideally python3.

This setup relies on docker, if not already installed:
```
curl get.docker.com | bash
```
Install docker-compose:
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

## Create the ELK stack:  
```
cd elk
docker-compose up
```

This launches three docker images, one for Elasticsearch, Logstash and Kiabana in a docker network.

This repo also has the kibana frontend for completeness, but isn't required and if a frontend isn't needed them remove it from the docker-compose file and the `elk/kibana` folder.


# Sending logs to Elasticsearch  
Two inputs to elasticsearch have been defined in `elk/logstash/pipeline/logstash.conf`, tcp and file.

## Tcp input:  
`cat </path/to/file.log> | nc localhost 5000`

## File input:
Move a log file into the folder `logstash/logs`.  


The file input will automatically update when new loglines are added and feed into elasticsearch, so symlinking is advised if you wish to use this feature. Additonally add/remove any inputs you desire to `logstash.conf`

The logs are stored in the index `logstash-YYYY-MM-d` in elasticsearch, depending on the date they were sent. 

It is likely you will want to define your own [grok filters](https://www.elastic.co/guide/en/logstash/current/plugins-filters-grok.html) in `logstash.conf` for your specific logfiles. The following is useful for standard grok patterns:
[https://github.com/elastic/logstash/blob/v1.4.2/patterns/grok-patterns](https://github.com/elastic/logstash/blob/v1.4.2/patterns/grok-patterns)

# Retreiving logs from Elasticsearch to python

Once sent, view `elasticsearch_to_python.ipynb` in [Jupyter notebook](https://jupyter.org/) and run the cells for python to read and conver the logs from elasticsearch. Alternatively, if you do not wish to use Jupyter notebook run the `.py` file using a python interpreter instead.

Then the python script  `elasticsearch_to_python` then has two functions:

`get_elasticsearch_json`: Retrieves logs in json format from elasticsearch. Since a single elasticsearch query cannot have more than 10,000 results by default and logfiles often have more than 10,000 the function uses elasticsearch scrolls and then returns a list of all the scrolls.
You can specify the type of logfile, the types are defined by the grok filters in `logstash/pipeline/logstash.conf`. You may want to do this because different log types will be in the logfile and log algorithms can perform once when given different types of logs at once.

`json_to_pandas`: Converts the logs in json format into a pandas dataframe which is readable by the machine learning algorithms.

In the docker-compose file we set `xpack.security.enabled=false` to make the elasticsearch work in machine learning AMIs on aws, which you may not want to do for security reasons on production.

# Outlier detction

Now we have transformed the logs they can be used for outlier detection, which for example is shown by the script `log_novelty_detection.py`

The script is designed to return statistical outliers in log files using a count vectoriser algorithm.

# Flask API
Flask is a micro web framework for python, and allows for easy setup of API calls to a python app.

I have provided a flask app `flask.py`, which simply runs an app which can then be used to call the functions defined in the previous two python scripts mentioned to run the log outlier model.

The flask app can be run in a terminal using:  
`python flask.py`

Train and output of model commands:
```
curl -X POST -F file=@testfile localhost:4000/post_file
curl localhost:4000/train
curl localhost:4000/result
```

The second command then returns the five log files which have the highest outlier (could make it return any number you want by giving an input). 




This repo uses a copy of the ipynb and py file for each script, if you want autosaving from ipynb to py check out this link:  
https://protips.maxmasnick.com/ipython-notebooks-automatically-export-py-and-html
